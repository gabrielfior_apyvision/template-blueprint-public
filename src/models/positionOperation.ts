import { OperationType } from './constants';
import { PositionShares } from './positionShares';
import { TokenInfo } from './tokenInfo';
import BigNumber from 'bignumber.js';

export class PositionOperation {
  constructor(
    // deposit | withdrawal | income | transfer_in | transfer_out | null_op
    public operation: OperationType,
    // unique identifier
    public positionIdentifier: string,
    // tokens sent from user to protocol
    public inputTokens: TokenInfo[],
    // tokens sent from protocol to user
    public outputTokens: TokenInfo[],
    // gas token amount spent for transaction
    public gasTokenAmount = BigNumber(0),
    public positionShareDetails: PositionShares[],
  ) {}
}
