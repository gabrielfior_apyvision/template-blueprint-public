import { MetadataStore } from "../meta/metadataStore";
import { RequestContext } from "../requestContext";
import { PositionOperation } from "./positionOperation";
import { PositionValue } from "./positionValue";
import { PositionValueContext } from "./positionValueContext";
import { TransactionDetails } from "./transactionDetails";
import { UserTransactionResults } from "./userTransactionResults";


export interface Blueprint {
  getContractName(): string;
  getBlueprintKey(): string;
  // This allows a child blueprint to be looked up via its parentID
  getParentBlueprintId(): string;
  getContext(): RequestContext;
  getUserTransactions(
    context: RequestContext,
    userAddresses: string[],
    fromBlock: number,
  ): Promise<UserTransactionResults>;
  classifyTransaction(context: RequestContext, txn: TransactionDetails): Promise<PositionOperation[]>;
  getPositionValue(positionContext: PositionValueContext): Promise<PositionValue>;
  getUserList(fromBlock: number): Promise<string[]>;
  syncMetadata(metadataStore: MetadataStore, lastSyncAt: number): Promise<number>; // returns last synced timestamp
  syncMetadataInterval(): number; // returns the number of seconds the system should sync after the last sync
}
