import BigNumber from 'bignumber.js';

export class PositionShares {
  constructor(
    public sharesIdentifier = '', // address of the receipt token, or if not receipt token, the address of the input token which will form the shares of the position
    public amountAdded = BigNumber(0), // amount of 'shares' added/removed in this transaction, it's positive if shares were added, negative if shares were removed
    public sharePriceUsd = 0, // price of one share in USD
    public positionBalance?: BigNumber, // use this value for the position balance field if populated in positionValue
    public isLiabilityPosition: boolean = false, // It will be true in some cases where the position represents a liability instead of an asset (i.e. aave debt token)
  ) {}
}
